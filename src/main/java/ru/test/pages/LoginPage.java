package ru.test.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.test.appmanager.PageManager;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class LoginPage extends Page {

    public LoginPage(PageManager pages) {
        super(pages);
    }

    @FindBy(id = "ips_username")
    private WebElement usernameField;

    @FindBy(id = "ips_password")
    private WebElement passwordField;

    @FindBy(xpath = "//input[@class='ipsButton']")
    private WebElement submitButton;

    @Step("Ввожу Имя пользователя")
    public LoginPage setUsernameField(String text) {
        usernameField.clear();
        usernameField.sendKeys(text);
        return this;
    }

    @Step("Ввожу пароль")
    public LoginPage setPasswordField(String text) {
        passwordField.clear();
        passwordField.sendKeys(text);
        return this;
    }

    @Step("Нажимаю на кнопку Войти")
    public void clickSubmitButton() {
        submitButton.click();
    }

    public LoginPage ensureModalLoaded() {
        wait.until(presenceOfElementLocated(By.xpath(String.format("//h3[text()='Войти']"))));
        return this;
    }
}
