package ru.test.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.test.appmanager.PageManager;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class MainPage extends Page{

    public MainPage(PageManager pages) {
        super(pages);
    }

    @FindBy(xpath = "//a[@id='sign_in']")
    private WebElement loginLink;

    @FindBy(xpath = "//a[@id='register_link']")
    private WebElement registrationLink;

    @FindBy(xpath = "//p[@class='message error']")
    private WebElement messageError;

    @Step("Страница открылась")
    public MainPage ensurePageLoaded() {
        wait.until(presenceOfElementLocated(By.xpath(String.format("//h3[text()='Войти']"))));
        return this;
    }

    public void clickLoginLink() {
        loginLink.click();
    }

    @Step("Кликаем на ссылку регистрации")
    public void clickRegistrationLink() {
        registrationLink.click();
    }

    public String getErrorMessageText() {
        return messageError.getText();
    }

}
