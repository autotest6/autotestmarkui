package ru.test.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.test.appmanager.PageManager;

public class RegistrationPage extends Page {
    public RegistrationPage(PageManager pages) {
        super(pages);
    }

    @FindBy(id = "display_name")
    private WebElement usernameField;

    @FindBy(id = "email_1")
    private WebElement emailField;

    @FindBy(id = "password_1")
    private WebElement passwordField;

    @FindBy(id = "password_2")
    private WebElement confirmPasswordField;

    @FindBy(id = "field_9")
    private WebElement purposeField;

    @FindBy(id = "agree_tos")
    private WebElement agreeCheckbox;

    @FindBy(id = "register_submit")
    private WebElement submitButton;

    public RegistrationPage setUsernameField(String text) {
        usernameField.clear();
        usernameField.sendKeys(text);
        return this;
    }

    public RegistrationPage setEmailField(String text) {
        emailField.clear();
        emailField.sendKeys(text);
        return this;
    }

    public RegistrationPage setPasswordField(String text) {
        passwordField.clear();
        passwordField.sendKeys(text);
        return this;
    }

    public RegistrationPage setConfirmPasswordField(String text) {
        confirmPasswordField.clear();
        confirmPasswordField.sendKeys(text);
        return this;
    }

    public RegistrationPage setPurposeField(String text) {
        purposeField.clear();
        purposeField.sendKeys(text);
        return this;
    }

    public RegistrationPage clickAgreeButton() {
        agreeCheckbox.click();
        return this;
    }

    public void clickSubmitButton() {
        submitButton.click();
    }

}
