package ru.test.pages;

import org.openqa.selenium.By;
import ru.test.appmanager.PageManager;

public class InternalPage extends Page {
    public InternalPage(PageManager pages) {
        super(pages);
    }

    public boolean isLoggedIn() {
        return driver.findElements(By.xpath("//a[text()='Выход']")).size() != 0;
    }
}
