package ru.test.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.test.appmanager.PageManager;


public abstract class Page {
    public static final int TIME_OUT_IN_SECONDS_FOR_FIND_WEB_ELEMENT = 10;
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected PageManager pages;

    public Page(PageManager pages) {
        this.pages = pages;
        driver = pages.getWebDriver();
        wait = new WebDriverWait(driver, TIME_OUT_IN_SECONDS_FOR_FIND_WEB_ELEMENT);
    }

    public WebDriver getWebDriver() {
        return driver;
    }

    public Page ensureModalLoaded() {
        return this;
    }
}
