package ru.test.model;

public class User implements Cloneable {

    private String username;
    private String password;
    private String email;
    private String purpose;

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPurpose() {
        return purpose;
    }

    public User setPurpose(String purpose) {
        this.purpose = purpose;
        return this;
    }

}
