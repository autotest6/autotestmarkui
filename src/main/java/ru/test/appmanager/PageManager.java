package ru.test.appmanager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import ru.test.pages.*;


public class PageManager {

    public static final int TIME_OUT_IN_SECONDS_FOR_FIND_WEB_ELEMENT = 10;
    private WebDriver driver;

    private MainPage mainPage;
    private InternalPage internalPage;
    private LoginPage loginPage;
    private RegistrationPage registrationPage;


    public PageManager(WebDriver driver) {
        this.driver = driver;

        mainPage = initElements(new MainPage(this));
        loginPage = initElements(new LoginPage(this));
        internalPage = initElements(new InternalPage(this));
        registrationPage = initElements(new RegistrationPage(this));
    }

    private <T extends Page> T initElements(T page) {
        PageFactory.initElements(new DisplayedElementLocatorFactory(driver, TIME_OUT_IN_SECONDS_FOR_FIND_WEB_ELEMENT), page);
        return page;
    }

    public WebDriver getWebDriver() {
        return driver;
    }

    public MainPage getMainPage() {
        return mainPage;
    }

    public LoginPage getLoginPage() {
        return loginPage;
    }

    public InternalPage getInternalPage() {
        return internalPage;
    }

    public RegistrationPage getRegistrationPage() {
        return registrationPage;
    }
}
