package ru.test.appmanager;

import io.qameta.allure.Step;
import ru.test.model.User;

public class UserHelper {
    private PageManager pages;

    public UserHelper(ApplicationManager manager) {
        this.pages = manager.getPages();
    }

    @Step("авторизоваваемся")
    public UserHelper loginAs(User user) {
        pages.getMainPage()
                .ensurePageLoaded()
                .clickLoginLink();
        pages.getLoginPage()
                .ensureModalLoaded()
                .setUsernameField(user.getUsername())
                .setPasswordField(user.getPassword())
                .clickSubmitButton();
        return this;
    }

    @Step("Проверка авторизации")
    public boolean isLoggedIn() {
        return pages.getInternalPage().isLoggedIn();
    }

    @Step("Регистрация нового пользователя")
    public UserHelper registerAs(User user) {
        pages.getMainPage()
                .ensurePageLoaded()
                .clickRegistrationLink();
        pages.getRegistrationPage()
                .setUsernameField(user.getUsername())
                .setEmailField(user.getEmail())
                .setPasswordField(user.getPassword())
                .setConfirmPasswordField(user.getPassword())
                .setPurposeField(user.getPurpose())
                .clickAgreeButton()
                .clickSubmitButton();
        return this;
    }
    public String getErrorMessageText() {
        return pages.getMainPage().getErrorMessageText();
    }
}
