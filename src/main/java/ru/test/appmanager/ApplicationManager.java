package ru.test.appmanager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.test.util.Browser;
import ru.test.webdriver.WebDriverFactory;

import java.io.File;

public class ApplicationManager {

    private NavigationHelper navivagationHelper;
    private UserHelper userHelper;

    private WebDriver driver;
    private String baseUrl;

    protected WebDriverWait wait;
    protected PageManager pages;

    public static final int TIME_OUT_IN_SECONDS_TO_WAIT_ELEMENT = 10;

    public ApplicationManager() {
        baseUrl = "https://software-testing.ru/forum/";

        Browser browser = new Browser();
        File DriverPath = new File("");
        System.setProperty("webdriver.chrome.driver", DriverPath.getAbsolutePath() + "\\tools\\chromedriver.exe");
        browser.setName("CHROME");

        driver = WebDriverFactory.getInstance(browser);
        wait = new WebDriverWait(driver, TIME_OUT_IN_SECONDS_TO_WAIT_ELEMENT);
        pages = new PageManager(driver);
        assert driver != null;

        navivagationHelper = new NavigationHelper(this);
        userHelper = new UserHelper(this);

        getNavigationHelper().openMainPage();
    }

    public void stop() {
        driver.quit();
    }

    public WebDriver getWebDriver() {
        return driver;
    }

    public NavigationHelper getNavigationHelper() {
        return navivagationHelper;
    }

    public UserHelper getUserHelper() {
        return userHelper;
    }

    protected PageManager getPages() {
        return pages;
    }

    protected String getBaseUrl() {
        return baseUrl;
    }

}
