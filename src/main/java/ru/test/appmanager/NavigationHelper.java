package ru.test.appmanager;

import org.openqa.selenium.WebDriver;

public class NavigationHelper {
    private final String baseUrl;
    private PageManager pages;
    private WebDriver driver;

    public NavigationHelper(ApplicationManager manager) {
        this.baseUrl = manager.getBaseUrl();
        this.driver = manager.getWebDriver();
        this.pages = manager.getPages();
    }

    public void openMainPage() {
        try {
            driver.get(baseUrl);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("NavHelper: can't open main page");
        }
    }
}
