package ru.test.base;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import ru.test.appmanager.ApplicationManager;
import ru.test.model.User;

import java.lang.reflect.Method;

public class TestBase {

    protected ApplicationManager app;

    protected static User correctUser = new User()
            .setUsername("TestLogin")
            .setPassword("TestLogin");

    protected static User wrongUser = new User()
            .setUsername("test123@test123.ru")
            .setPassword("test");

    protected static User emptyLoginAndPassword = new User()
            .setUsername("")
            .setPassword("")
            .setEmail("")
            .setPurpose("");

    protected static User newUser = new User()
            .setUsername("TestLogin1")
            .setPassword("TestLogin2")
            .setEmail("test@test1.test")
            .setPurpose("some text added by autotest");

    @BeforeClass
    public void init() {
        if (app == null) {
            app = new ApplicationManager();
        }
    }

    @AfterClass
    public void stop() {
        app.stop();
    }

    @BeforeMethod
    public void openMainPage(Method method) {
        app.getNavigationHelper().openMainPage();
    }
}
