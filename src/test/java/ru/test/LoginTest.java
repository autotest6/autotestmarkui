package ru.test;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.test.base.TestBase;

public class LoginTest extends TestBase {

    @Test
    public void test0LoginFailed() throws Exception {
        app.getUserHelper().loginAs(wrongUser);
        Assert.assertFalse(app.getUserHelper().isLoggedIn());
        Assert.assertEquals(app.getUserHelper().getErrorMessageText(), "Логин или пароль неверны.");
    }

    @Test
    public void test1LoginFailed() throws Exception {
        app.getUserHelper().loginAs(emptyLoginAndPassword);
        Assert.assertFalse(app.getUserHelper().isLoggedIn());
        Assert.assertEquals(app.getUserHelper().getErrorMessageText(), "Необходимо заполнить форму полностью. Пожалуйста вернитесь назад и проделайте это.");
    }
    @Test
    public void test2ManagerLoginOK() throws Exception {
        app.getUserHelper().loginAs(correctUser);
        Assert.assertTrue(app.getUserHelper().isLoggedIn());
    }
}
