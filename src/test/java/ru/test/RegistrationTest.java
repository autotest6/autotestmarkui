package ru.test;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.test.base.TestBase;


public class RegistrationTest extends TestBase {

    @Test
    public void test0RegistrationFailed() throws Exception {
        app.getUserHelper().registerAs(emptyLoginAndPassword);
        Assert.assertFalse(app.getUserHelper().isLoggedIn());
        Assert.assertEquals(app.getUserHelper().getErrorMessageText(), "В процессе проверки регистрации были найдены ошибки в заполнении формы. Пожалуйста исправьте их и продолжите регистрацию.");
    }

    @Test
    public void test1RegistrationOk() throws Exception {
        app.getUserHelper().registerAs(newUser);
        Assert.assertTrue(app.getUserHelper().isLoggedIn());
    }
}
